package com.kingsman;

import com.google.auth.Credentials;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.Bucket;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import org.json.JSONObject;
import redis.clients.jedis.Jedis;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Comparator;

public class Stre {

    public static void main(String[] args) throws InterruptedException {
        final String REDIS_SERVER_HOST = "172.31.252.134";
        final int REDIS_SERVER_PORT = 6379;

        Jedis jedis = new Jedis(REDIS_SERVER_HOST, REDIS_SERVER_PORT);

        String postgresUrl = "jdbc:postgresql://34.132.109.72:5432/postgres";
        String postgresUser = "kingsman";
        String postgresPassword = "uuuu";
        String postgresQuery = "SELECT * FROM weatherconfig LIMIT 1";

        Blob oldBlob = null;
        try {
            // Load the JSON credentials file
            FileInputStream serviceAccountStream = new FileInputStream("src/main/resources/secretsGcp.json");
            Credentials credentials = GoogleCredentials.fromStream(serviceAccountStream);

            // Set up the storage service with the credentials
            Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();

            while (true) {
                Thread.sleep(500);
                try (Connection connection = DriverManager.getConnection(postgresUrl, postgresUser, postgresPassword)) {
                    // Google Cloud Storage setup
                    String bucketName = "kingsman-weather-watcher"; // Replace with your GCS bucket name
                    String directoryName = "weather-data"; // Directory name

                    Bucket bucket = storage.get(bucketName);

                    // List all blobs in the specified directory
                    Iterable<Blob> blobs = bucket.list(Storage.BlobListOption.prefix(directoryName)).iterateAll();

                    // Find the latest blob by comparing modification timestamps
                    Blob latestBlob = null;
                    long maxUpdateTime = Long.MIN_VALUE;

                    for (Blob blob : blobs) {
                        if (!blob.getName().endsWith("/")) { // Filter out subdirectories
                            long updateTime = blob.getUpdateTime();
                            if (updateTime > maxUpdateTime) {
                                maxUpdateTime = updateTime;
                                latestBlob = blob;
                            }
                        }
                    }

                    PreparedStatement preparedStatement = connection.prepareStatement(postgresQuery);
                    ResultSet resultSet = preparedStatement.executeQuery();
                    int id ;
                    int temperatureMin = 0;
                    float humidityMax=0 ;
                    int windSpeedMax=0 ;
                    int rainMax=0 ;
                    while (resultSet.next()) {
                        id = resultSet.getInt("id");
                        temperatureMin = resultSet.getInt("temperature-min");
                        humidityMax = resultSet.getFloat("humidity-max");
                        windSpeedMax = resultSet.getInt("wind-speed-max");
                        rainMax = resultSet.getInt("rain-max");
                    }

                    // Read the content of the latest blob
                    if (latestBlob != null && !latestBlob.equals(oldBlob)) {
                        String latestDocumentContent = new String(latestBlob.getContent(), StandardCharsets.UTF_8);
                        System.out.println(latestDocumentContent);

                        // Parse the JSON content
                        // Example: Assuming JSON structure like {"avg_temperature": 25.5, "avg_humidity": 50.0, ...}
                        JSONObject json = new JSONObject(latestDocumentContent);

                        // Get the required values from the JSON
                        double avgTemperature = json.getDouble("avg_temperature");
                        double avgHumidity = json.getDouble("avg_humidity");
                        double avgWindSpeed = json.getDouble("avg_wind_speed");
                        double avgPrecipitation = json.getDouble("avg_precipitation");

                        // Perform your custom comparisons
                        boolean temperatureComparison = avgTemperature >= temperatureMin;
                        boolean humidityComparison = avgHumidity <= humidityMax;
                        boolean windSpeedComparison = avgWindSpeed <= windSpeedMax;
                        boolean precipitationComparison = avgPrecipitation <= rainMax;

                        // Example: If any comparison is false, send to Redis
                        if (!temperatureComparison || !humidityComparison || !windSpeedComparison || !precipitationComparison) {
                            jedis.set("watering-command-automatic", "on");
                            System.out.println("on");
                        }
                    }
                    oldBlob = latestBlob;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}